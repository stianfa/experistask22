﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using calculator;
using Xunit;

namespace myTests
{
    public class SlNameTests
    {
        [Fact]
        public void Add_testCorrectSum()
        {
            Assert.Equal(4, Calculator.Add(2, 2));
        }

        [Theory]
        [InlineData(10, 2, 12)]
        [InlineData(-4, -6, -10)]
        [InlineData(-2, 2, 0)]
        [InlineData(int.MinValue, -1, int.MaxValue)]
        public void CanAddTheory(int value1, int value2, int expected)
        {
            Assert.Equal(expected, Calculator.Add(value1, value2));
        }

        [Fact]
        public void Divide_testZeroDivision()
        {
            Assert.Throws<Exception>(() => Calculator.Divide(8, 0));

        }

        [Fact]
        public void Divide_testCorrectSum()
        {
            Assert.Equal(4, Calculator.Divide(8, 2));

        }

        [Fact]
        public void Subtract_testCorrectSum()
        {
            Assert.Equal(4, Calculator.Subtract(6, 2));
        }

        [Fact]
        public void Multiply_testCorrectSum()
        {
            Assert.Equal(9, Calculator.Multiply(3, 3));
        }
    }
}
